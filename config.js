module.exports = {
    token: 'garabatowebtokensecreto', 
    host: process.env.OPENSHIFT_MONGODB_DB_HOST || '127.0.0.1',
    portdb:'27017',
    db:'user',
    ip_addr : '127.0.0.1',
    port    : '8080'
};